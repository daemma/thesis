% chapter_elp.tex {Anisotropy : Estimating Little-p}
\chapter{Anisotropy : \\ Estimating Little-p} \label{chap:elp}

\section{Chapter Overview} 
The text in this chapter was published under the title ``The Flux at different `Angles''' 
by the authors B. R. Becker, S. BenZvi, B. Connolly, M. S. Gold, E. J. Hague, J. A. J. Matthews
and S. Westerhoff in on the internal Auger technical notes server as {\em GAP-Note-2007-097}.
This paper was written just after the so called ``running prescription'' passed\cite{antoine}, 
which allowed ``the correlation of ultra high energy CR with Active Galactic Nuclei''\cite{refPAOscience}. 
The primary goal of this work is to estimate the observed value of the correlation {\em little-p} in post-scan data. 
This work was not widely used at the time it was published, but has since become the analysis basis for 
some of the key results of later Auger work (see \secref{sec:cps}). 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} \label{sec:intro}
Given that the {\it running-prescription}\cite{gap096} has passed\cite{antoine},
{\it viz.} that 6 of the first 8 events correlated, it is
timely to ask what values of little-$p$ are consistent with our
observations. 
For this note the most relevant parts of the {\it prescription} are:
\begin{itemize}
\item {\bf $n$}, the total number of events with energy greater than
  56 EeV, 
\item {\bf $k$}, the number of events (out of $n$) that correlate with the
  angular region of the sky defined in the {\it prescription}, 
  {\it i.e.} $\gamma_{max} = 3.1^{o}$ and $z_{max} = 0.018$, 
\item {\bf little-$p$}, the fractional probability of any specific outcome
  {\it e.g.} the per cosmic ray probability that it passes the {\it prescription} and 
\item {\bf little-$p_{bg}=0.21$}, the probability for an accidental (or chance) cosmic ray 
  passing the {\it prescription}. If the cosmic rays follow the
  acceptance of the Auger experiment, then little-$p$=0.21.
\end{itemize}

In this note we use the {\it alternative prescription}
conventions outlined in Connolly {\it et.al.}\cite{gap096}, which does
not ``pass'' until $k/n = 8/11$. However, the discussion of our
estimate of little-$p$ is relevant to both schemes since it does not
depend on how one chooses to reject the isotropic (null) hypothesis.

The likelihood of getting k correlations out of n events is the
binomial term\cite{gap096},
\begin{linenomath*}
\begin{equation}
\label{eq:L}
L( k | n, p ) = \frac{n!}{k!(n-k)!} p^{k} (1-p)^{n-k}.
\end{equation}
\end{linenomath*}
In this note will choose a flat prior, $\pi(p) = 1$ for $0 < p < 1$. 
Intuitively, this choice reflects a neutral stance as to how many
events $k$ we {\it expect} out of the total $n$. We leave the
discussion of different choice of prior to future communique. 

Following Bayes' Theorem \cite{refPDG} we can write our belief about the parameter little-$p$ {\it given} that $k$
out of $n$ events correlate as,
\begin{linenomath*}
\begin{equation}
\label{eq:Post}
Post(p | n, k) = \frac{(n+1)!}{k!(n-k)!} p^{k} (1-p)^{n-k}.
\end{equation}
\end{linenomath*}
This {\it posterior} distribution tells the whole story about ones
belief about little-$p$ after the data is collected.
Indeed, no matter the evidence reported by the experimenter, ``... the consumer of that result will almost
certainly use it to derive some impression about the value of the parameter. This will
inevitably be done, either explicitly or intuitively, with Bayes'
theorem ...''\footnote{Quoted from the {PDG}\cite{refPDG} $\S${\it 32 Statistics}. }
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Estimating little-$p$}\label{sec:elp}
In order to gain intuition about $Post(p | n, k)$ we can quantify 
its characteristic values; ``location'' $\hat{p}$ and ``shape'' $\delta p_{\pm}$.
The maximum of eq(\ref{eq:Post}) occurs at $\hat{p} = k/n$. 
We may consider $\hat{p}$ the estimated value of little-$p$ and
associate it with the location of $Post(p | n, k)$.
We determine the error of this estimate ({\it i.e.} the shape of
$Post(p | n, k)$) in two different ways.

The first is based on the expected error in the observed number of
events $k$. In the limit of large $n$, the variance of $k$ for $L( k |
n, p )$ (see eq(\ref{eq:L})) is $(\delta k)^{2} =  n p (1-p) $. From the fact that
$\hat{p} = k/n$ we can write $\delta p \sim (\delta k)/n$, which
simplifies to 
\begin{linenomath*}
\begin{equation}
\label{eq:dp}
\delta p = \sqrt{ \frac{k(n-k)}{n^{3}}}. 
\end{equation}
\end{linenomath*}
This gives a approximate and symmetric error for our estimate of little-$p$.

The second method directly uses the posterior and gives an asymmetric interval about $\hat{p}$.
We define the $1$-standard-deviation ({\it i.e.} $1\sigma$) confidence
interval for $\hat{p}$ such that $Post(p | n, k)$ (eq(\ref{eq:Post})) has the same value
when it is evaluated at lower-limit and at the upper-limit {\bf and} that the
integral of $Post(p | n, k)$ from the lower- to the upper-limit is
equal to the (symmetric) area under the standard normal curve.
Numerically, we find the interval 
$\hat{p} - \delta p_{-} \leq p \leq \hat{p} + \delta p_{+}$, such that
 $\delta p_{-}$ and $\delta p_{+}$ satisfy: 
\begin{linenomath*}
\begin{subequations}
\begin{align}
Post(\hat{p} - \delta p_{-} | n, k) &= Post(\hat{p} + \delta p_{+} | n, k) \label{eq:cond1} \\
\int_{\hat{p} - \delta p_{-}}^{\hat{p} + \delta p_{+}} Post(p | n, k) dp &= \int_{-1}^{1} e^{-t^{2}/2} / \sqrt{2\pi} dt \sim \%68.3.\label{eq:cond2}
\end{align}
\end{subequations}
\end{linenomath*}
The interpretation of this interval is that {\it given} that $k$ out
of $n$ events correlate, there is a $\%68.3$ chance
that the true little-$p$ falls between $\hat{p} - \delta p_{-}$ and $\hat{p} + \delta p_{+}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Consistency with Background}\label{sec:cwb}
Under the scheme discussed in this note, the proper method of accepting or
rejecting the background (isotropic) hypothesis is outlined in
\cite{gap093}. To this end, equation five on page seven of \cite{gap093} defines a test statistic $R'$,
\begin{linenomath*}
\begin{equation}
\label{eq:Rp}
R' = \frac{ \frac{k!(n-k)!}{(n+1)!} }{ 0.21^{k}(1-0.21)^{n-k}  } = 
     \frac{1}{Post(p_{bg}=0.21 | n, k)}.
\end{equation}
\end{linenomath*}
This illustrates the connection between isotropic hypothesis rejection
($R'$) and our posterior belief about little-$p$ given $k$ and $n$ ($Post(p | n, k)$).

For completeness, we review the three possible outcomes\cite{gap093} for this test
statistic; if (1) $R' \geq 95$ then we can reject the isotropic hypothesis with a
probability $\alpha = \%1$, (2) $0.051<R'< 95$ then
we can neither accept nor reject the hypothesis of isotropy, (3) then 
$R'<0.051$ then we can accept the hypothesis with a probability $\beta = \%5$.

\section{Results}s\label{ssec:results}
Here we present a graphical and numerical summary of the aforementioned methods
applied to the post-{\it Prescription}-definition\cite{gap096} P.A.O. data.
For example, {\it given} that 6 out of 8 events correlate, the first
row of Table \ref{table:ci} tells us the following; 
\begin{itemize}
\item little-$\hat{p}$ ($3^{rd}$ column): the ``best estimate'' of little-$p$ is $\hat{p}
  = k/n = 0.750$,
\item $^{+\delta p_{+}}_{-\delta p_{-}}$ ($4^{th}$ column): the probability that the
  true value of little-$p$ is in the interval $0.750^{+0.122}_{-0.157}$ is
  approximately $\%68.3$, see eqs(\ref{eq:cond1}, \ref{eq:cond2}), 
\item $\delta p$ ($5^{th}$ column): the symmetric error of $\hat{p}$, see
  eq(\ref{eq:dp}) and 
\item $R'$ ($6^{th}$ column): $R' = 74.14$ and thus we can
  neither accept nor reject the isotropic hypothesis (see \cite{gap093} for details). 
\end{itemize}


\vspace{1cm}
\begin{table}[h]
\begin{center}
\begin{tabular}{c c || c c c | c   }
\hline
k & n  & little-$\hat{p}$ & $^{+\delta p_{+}}_{-\delta p_{-}}$ & $\delta p$ & $R'$  \\
\hline
 6 & 8  & $0.750$ & $^{+0.122}_{-0.157}$ & $0.153$ & $74.14$ \\
 7 & 10 & $0.700$ & $^{+0.122}_{-0.145}$ & $0.145$ & $85.31$ \\
 8 & 11 & $0.727$ & $^{+0.113}_{-0.137}$ & $0.134$ & $270.83^{\dag}$  \\ %
% 8 & 12  & $0.667$ & $^{+0.118}_{-0.135}$ & $0.136$ & $105.48$ \\
 8 & 13 & $0.615$ & $^{+0.121}_{-0.131}$ & $0.135$ & $47.69$ \\
\hline
\end{tabular}
\end{center}
\caption[For $k$ out of $n$ events correlating we calculate 
  little-$\hat{p}$ as the estimated value.]{For $k$ out of $n$ events correlating we calculate 
  little-$\hat{p}$ is the estimated value, $^{+\delta p_{+}}_{-\delta p_{-}}$ is the (asymmetric) $\%68.3$
  confidence interval, $\delta p$ is a symmetric
  confidence interval based on $\delta k$ and $R'$ is the test statistic
  for rejection of the isotropic hypothesis.
  {\bf Note} that one may only read one row of this table to make
  inferences about the data. $^{\dag}$This means that $R'\geq 95$, at this point we can reject the isotropic
  hypothesis\cite{gap093}, with $\alpha=\%1$ probability.}
\label{table:ci}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}\label{sec:conc} 
The posterior distribution $Post(p | n, k)$ defined in eq(\ref{eq:Post}) and plotted
for the Auger data in Fig.\ref{fig:ci} tell the whole story of ones
belief(s) about little-$p$ given that $k$ out of $n$ events correlate. 
One may use only one row of Table \ref{table:ci} to gain a numerical
understanding of the location and shape of this posterior distribution.
Despite this, it is interesting to note that $\delta p$ slowly
decreases as $n$ increases. Also of note is that the {\it alternative 
prescription}\cite{gap093} ``passes'' at $8/11$ but at $8/13$ the
evidence against the null hypothesis has decreased.

\begin{sidewaysfigure}[htbp]
	\begin{center}
		\begin{tabular}{cc}
			\includegraphics*[width=0.5\linewidth]{Figs/cLittleP_6outa8} & 
			\includegraphics*[width=0.5\linewidth]{Figs/cLittleP_7outa10} \\
			\includegraphics*[width=0.5\linewidth]{Figs/cLittleP_8outa11} &
			\includegraphics*[width=0.5\linewidth]{Figs/cLittleP_8outa13} \\
		\end{tabular}
	\end{center}
	\caption[For each $n$ and $k$, {\it i.e.} each plot 
	the prior distribution $\pi(p)$ is the dashed black line.]{For each $n$ and $k$, {\it i.e.} each plot; 
	the prior distribution $\pi(p)$ is the dashed black line (see $\S$\ref{sec:intro}), 
	the posterior distribution for little-$p$ $Post(p | n, k)$ is the
	solid black line (see eq(\ref{eq:Post})), the $\%68.3$ confidence interval for little-$p$
	is plotted in {\color{blue}solid blue} (see eqs(\ref{eq:cond1}, \ref{eq:cond2}))
	and $1/R'$ is plotted as a {\color{red}red star} (see eq(\ref{eq:Rp})).
	\label{fig:ci}}
\end{sidewaysfigure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Chapter Post-Script}\label{sec:cps} 
As mentioned in the overview, this work on estimating little-$p$ has been an important building block 
for the analysis used to monitor the signal with data that arrived after the start of the prescription. 
In particular, one may construct an estimate of little-$p$ and its uncertainty for each new event that arrives. 
Each new estimate is correlated with the prior estimate in such a way that ones current understanding 
of the observed correlation is given by the current estimate of little-$p$. 
The update of the correlation with AGN using data collected after \cite{refPAOscience} is included as an appendix to this 
thesis (see \chpref{chap:agn}) and a figure showing the evolution of the observed value of little-$p$ can be found in 
right panel of \figref{fig:seq}. 




