% chap1.tex {Introductory Chapter}
\chapter{Introduction}

\section{The Origins of CR Observation}
To appreciate any serious endeavor, it is invaluable to understand its history and context. 
There is a large body of historical work in the field of cosmic ray (CR) physics.  But, rather than attempt to summarize it here, I will 
review some selected historical events that I find particularly appealing and relevant to 
{\em ultra high} energy cosmic rays. 

Simply understanding the origin of the term ``cosmic ray'' reveals a great deal about 
the history of the field. 
Around the turn of the $20^{\text{th}}$ century it was believed that the observed ionization of the atmosphere was caused by
radiation from decaying radon in the Earth; ``atmospheric electricity'' supplies the ``ray'' in ``cosmic ray.''
Indeed, measurements made in the first decade of the 1900's (at building-level altitudes) could be explained by a model 
with the Earth as a source. 
In 1912, however, Victor Hess carried out detailed and higher-altitude balloon-based observations that allowed him to 
report a four-fold increase over the amount of radiation observed on the ground -- clearly inconsistent with an Earth-based source. 
(His appealingly adventurous spirit is on display in \figref{fig:hess}.)
Later, Robert Millikan proved that these rays are ``cosmic'' in origin, thus coining the term. \cite{refCRwiki} 

\begin{figure}[htbp] 
  \begin{center}
    \includegraphics*[width=0.25\linewidth]{Figs/viktorhess_001} 
  \end{center}
  \caption[Victor Hess in a balloon.]{\label{fig:hess}
    Victor Hess in a balloon\cite{refhesspic}.}
\end{figure}

As the field of CR physics developed, ``atmospheric electricity'' came to be understood as extensive air showers, 
the origins of which could be traced to single particles interacting with the atmosphere; 
for the highest-energy primaries -- in the regime addressed by this thesis -- these showers can 
be tens of square kilometers in size by the time they reach the ground. 
With the technological advances that led to the assembly of detector-arrays, the reconstruction of the 
qualities of the primaries from the measured quantities of the shower became possible.
The crucial advance for modern ultra high energy detectors was the 
development of accurate and fast read-out timing capabilities, which allowed the experimenter to confidently identify single, large-area showers and to mathematically reconstruct the shower front. 

The University of New Mexico played an important role in the modern development of the field. 
As the primary member of the Volcano Ranch experiment (1959-1976) set just west of Albuquerque, UNM Researcher John Linsley 
reported the first cosmic ray primary with energy $\sim 10^{20}$~eV\cite{refLinsley}. 
(Linsley is pictured in \figref{fig:linsl} below checking for snakes in one of the detectors at Volcano Ranch).
\begin{figure}[htbp] 
  \begin{center}
    \includegraphics*[width=110mm, height=60mm]{Figs/volcano_ranch-72dpi} 
  \end{center}
  \caption[John Linsley checking for rattle snakes near a Volcano Ranch detector.]{\label{fig:linsl}
    John Linsley checking for rattle snakes near a Volcano Ranch detector, outside Albuquerque, NM \cite{reflinlspic}.}
\end{figure}
\begin{figure}[htbp] 
  \begin{center}
    \includegraphics*[width=0.50\linewidth]{Figs/volcanoranchevent} 
  \end{center}
  \caption[A schematic of a shower observed by Volcano Ranch array.]{\label{fig:linsl}
     A schematic of a shower observed by Volcano Ranch array\cite{Linsley:1963km}. The primary that triggered this shower had
 	an energy greater than $10^{20}$~eV.}
\end{figure}

We now understand that the origin of the low-energy cosmic rays observed by Hess (and scientists at modern and sophisticated facilities)
is different than the high energy events observed by Linsley (and at observatories since Volcano Park). 
But, while observations of low-energy cosmic rays are well-explained by particles accelerated within our solar system,  
the origin of the cosmic rays observed at the highest energies is still unknown.  

This question of ultra high energy cosmic ray-origination has been the subject of decades of theoretical inquiry and 
observational research by many large, and often international, collaborations. 
The core of the work presented within these pages is meant to contribute a verse to this dialog by describing empirically-motivated 
data analysis tools designed to supply direct answers to the questions surrounding ultra high energy cosmic ray origination. 

\section{Thesis Summary}
At the heart of high energy CR physics lies the question
\begin{center}
	{\em From where do the the highest energy cosmic rays originate?}
\end{center}
It is well beyond the scope of this thesis to supply an answer.  But, by allowing the data to speak as loudly as possible, the analysis tools presented here admit an advance toward the resolution of this fundamental issue.

There are three primary observations relevant to the characterization of high energy CR: 
the energy spectrum, the arrival direction, and the primary composition. 
Studies of CR anisotropy and the energy spectrum will be the focus of these pages. As for the composition of CR, topical discussion will be included where relevant, but interested readers should see \cite{ref:Smith} for a more thorough review.

Starting with \chpref{chap:cont}, the theoretical context for the observation of the energy spectrum and arrival directions is presented, and the status of current observations -- which suggest consistency with a theoretical model wherein charged particle primaries originate from outside our galaxy -- is reviewed.  As will be clear there, despite the fact that incredibly high energies 
and (relatively) small magnetic fields are also involved, it is the charge of the primaries that makes CR astronomy so difficult.  Given enough statistics (1000's of events) the sources can be identified (as depicted in \figref{fig:magdefl}, \secref{cont:sec:mf}).  However, the flux of high energy CR is incredibly low ($~1$ event/km$^{2}$/century) and is (presumably) further limited by the so-called GZK effect (see \secref{cont:ssec:gzk}). 

\chpref{chap:tpao} begins with an outline of the particle physics phenomenology of air showers created by CR entering the atmosphere of Earth, and continues with discussions devoted to the reconstruction of the energy and arrival 
direction of the CR primary from data collected at the Pierre Auger Observatory (Auger). 
Except for \chpref{chap:cfsad}, all remaining chapters are presented as previously published elsewhere. 
In these Chapters, statistics designed to confirm or deny the presence of flux suppression at the highest energies (i.e. the GZK effect) are developed, explored, and applied (see \ref{chap:fsba} and \ref{chap:fsua}).  
%Chapters \ref{chap:fsba} and \ref{chap:fsua} develop, explore and apply statistics designed 
%to confirm or deny the presence of flux suppression at the highest energies (i.e. the GZK effect). 
Most interestingly, these chapters describe the {\em Tail Power Statistic}, which has the capacity to provide information about flux suppression {\em independent} of the underlying power-law index.  \chpref{chap:fsba} (based on a manuscript published in the peer-reviewed journal Astroparticle Physics \cite{refHague}) explores the binned approach to the so-called TP statistic, and describes the application of this approach to both AGASA and preliminary Auger spectra.  The un-binned approach, discussed and applied to simulated CR data sets in \chpref{chap:fsua}, allows more precise parameter estimation, as well as the natural incorporation of 
systematic and statistical energy errors into analysis.  The results and discussion presented in this Chapter (\chpref{chap:fsba}) appear as published in the peer-reviewed journal Astroparticle Physics \cite{refHague}.
%The un-binned approach is discussed in \chpref{chap:fsua} and applied to simulated CR data sets.

Next, in \chpref{chap:cfsad} an attempt is made to directly compare the spectra observed in different 
regions of the sky using Auger data. 
In particular, the characteristic energy at which the spectrum ``cuts-off'' is studied for each of these spectra.  

With \chpref{chap:elp}, the portion of this thesis dedicated to CR energy spectra is exchanged for the topic of CR anisotropy.  In 2008, Auger published a ``correlation of [high energy CR] with active galactic nuclei'' \cite{refPAOscience,Abraham:2007si}, a claim given careful attention by the entire collaboration. 
In \chpref{chap:elp} a Bayesian scheme to estimate the degree of correlation is introduced, which was later developed and applied as a part of the update of \cite{refPAOscience,Abraham:2007si}. Appendix \ref{chap:agn} reproduces this work, as intended for publication in the Proceedings of the 31$^{\text{st}}$ ICRC. 

In light of the correlation-claim by Auger based on the use of a particular catalog of known objects, in \chpref{chap:cim} four {\em catalog independent} methods for detecting anisotropy are described and applied to both 
simulated and actual CR arrival directions. 
The most interesting and powerful of these methods uses a {\em shape-strength} parameterization of triplets of arrival directions  
and can distinguish between possible point-like and string-like anisotropies. 
This method is developed further in \chpref{chap:ss}, where it is also closely compared with a similar method that uses doublets of events.  Finally, a summary of this thesis is presented in \chpref{chap:suma}.  









