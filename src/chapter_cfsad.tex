% chapter_cfsad.tex {Connecting Flux Suppression with Arrival Directions}
\chapter{Connecting Flux Suppression with Arrival Directions} \label{chap:cfsad}

\section{Chapter Overview} 
The text in this chapter was intended for submission under the title ``The Flux at different `Angles''' 
by the authors E. J. Hague, B. R. Becker, M. S. Gold and J. A. J. Matthews in 
on the internal Auger technical notes server {\em GAP-Notes}.
This paper is intended as a first attempt at extracting and comparing spectral information 
for events with different arrival directions in the sky (and the temporal coordinate). 
This work has not been previously published. 

The spectrum in six different ``angular'' coordinates is studied; 
zenith, azimuth, time, declination, angular distance from the 
galactic plane, the super galactic planes and Centaurus A. 
We bin the events by angle into equal event number bins 
and fit the fluxes to the single and double power-laws. 
We concentrate on characterizing the consistency of the 
highest energy flux between the angular bins.
We find anomalous high energy flux for the azimuth between 
-3 and 89 degrees and within 44 degrees of Centaurus A. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} \label{sec:Intro}
Of primary interest to the Auger Observatory (PAO) is the study 
of the spectrum the highest energy cosmic rays. 
The PAO has reported strong evidence in favor of both the GZK-cutoff \cite{ref:PAOprl} and 
anisotropy \cite{refPAOscience,Abraham:2007si} at the highest energies. 
With this in mind, we endeavor here to qualatatively describe the flux 
of cosmic rays (CRs) in different regions of the sky. 

We begin with a brief description of how the energy is obtained from the observed 
quantities with an emphasis on the zenith angle dependence of the reconstructed energy. 
The methods we apply to this data set are described in \secref{sec:Method}; 
we fit spectra from four angular bins in six different ``angles''
\!\!\!\footnote{Time is not an angle.}\! 
to two different flux models. 
We summarize our observations in \secref{sec:Results} and 
a graphical and numerical summary of the fit results can also be found there.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data Set} \label{sec:Data}
%% -----------------------------------------
\subsection{Observer} \label{sec:Observer}
For this study we use the $\offline$ Observer surface detector data\cite{ref:Offline} 
that satisfy the following standard cuts: Bad Period ``$\$5<1$'', 
T5 ``$\$10==1 \,\,\|\,\, \$10==3$'', Zenith angle $\theta$ ``$\$17>0 \,\,\&\&\,\, \$17<60$'' 
and Energy $E$/EeV$\geq 10^{0.5}$.

At the time of writing, the energies of each event in the Observer file\cite{ref:Offline} 
have been reconstructed using parameters derived for ICRC `05\cite{ref:PAOicrc2005}. 
The reconstruction methods and parameters have since been refined and updated to include 
more contemporary data. 
We use the parameters and methods found in the Physics Review Letters 
article\cite{ref:PAOprl}. 

%% -----------------------------------------
\subsection{Constant Intensity Cut} \label{sec:CIC}
To calculate the energy using the Constant Intensity Cut (CIC) method we need two 
measured quantities; the zenith angle $\theta$ in 
degrees and the signal one thousand meters from the shower core $\Sone$ in vertical 
equivalent muons VEM. A histogram of these two measured quantities for this data set 
can be found in \figref{fig:C2S1000}. 
\begin{figure}[htbp] 
  \begin{center}
    \includegraphics*[width=110mm, height=60mm]{Figs/cC2S1000} 
  \end{center}
  \caption[	A two dimensional histogram of the data as a function of the square 
  of the cosine of the zenith angle $\cos^{2}(\theta)$ and signal $\Sone$.]{\label{fig:C2S1000}
    A two dimensional histogram of the data as a function of the square 
    of the cosine of the zenith angle $\cos^{2}(\theta)$ and signal $\Sone$.
    These two quantities are used to reconstruct the energy.}
\end{figure}

Using the attenuation curve $f_{Att}$ we can ``reconstruct'' the energy as
\begin{linenomath*}
  \begin{equation} \label{equ:E}
    E(\Sone, \theta ; a, b, A, B) = A \left( 
    \frac{\Sone}{f_{Att}(\theta ; a,b)} \right)^{B}, 
  \end{equation}
\end{linenomath*}
where $f_{Att}(\theta ; a,b) = 1+ax+bx^2$ with $x=\cos^2(\theta)-\cos^2(38^o)$,  
$a=0.92$, $b=-1.13$, $A=0.149$EeV and $B=1.080$\cite{ref:PAOprl}. 
The CIC method makes two assumptions about the flux of CRs; 
first the isotropy hypothesis, that ``the number of events of a given energy 
arriving from a given solid angle around a direction in the sky is the same 
for all directions''\cite{ref:PAOprl}, and second the hypothesis of the energy 
{\it independence} of \equref{equ:E}. 
The parameters $a$ and $b$ are are determined by fitting the attenuation 
curve to the data for a given intensity,  
$I_{0} = 0.24/\,\text{km}^{2}\,\text{sr}\,\text{yr} \approx 
9\text{EeV} \approx 47\text{VEM}$\cite{ref:Email}. 
Note that we use the exposure 
$\Omega = 7000 \,\text{km}^{2}\,\text{sr}\,\text{yr}$
quoted in \cite{ref:PAOprl}.
The parameters $A$ and $B$ are derived using hybrid events. 

In \figref{fig:NewEVsOldE} we compare the energy of each 
event reported in the Observer with the energy obtained using \equref{equ:E}. 
We note the the energies are highly corelated and that the relative difference 
in the energies can be substantial; $\sim20\%$ of the events have thier energy 
shifted by more than $\sim10\%$.
\begin{figure}[htbp] 
  \begin{center}
    \begin{tabular}{c c}
      \includegraphics*[width=0.5\linewidth]{Figs/cNewEVsOldE} & 
      \includegraphics*[width=0.5\linewidth]{Figs/cdEoE} 
    \end{tabular}
  \end{center}
  \caption[For each event we plot the log of the energy reported in the Observer 
  versus the log of the energy obtained using \equref{equ:E}.]{\label{fig:NewEVsOldE}
    {\it Left:} For each event we plot the log of the energy reported in the Observer 
    versus the log of the energy obtained using \equref{equ:E}.
    {\it Right:} The distribution of the relative difference between the energy reported 
    in the Observer\cite{ref:Offline} and the energy obtained using \equref{equ:E}.}
\end{figure}

The final and most substantial cut that we apply to the data set is that the 
energy obtained using \equref{equ:E} be greater than $10^{0.5}$EeV. 
There are a total of 17,361 events that satisfy all of our cut criteria. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Method} \label{sec:Method}
%% -----------------------------------------
\subsection{Models} \label{sec:Models}
In this study we fit various subsets of the energy spectrum to two 
models; the single and double power-laws, see \tabref{tab:models}. 
The single power-law has one free parameter; the spectral index $\tg$. 
The double power-law has three; a low energy index $\tg$, a characteristic 
break energy $\teb$/EeV and a high energy index $\td$. 
We use the un-binned maximum likelihood methods outlined in \cite{refHague,refHagueicrc1217} and \chpref{chap:fsua} 
to estimate the best fit parameters of each model. 
The statistical error of each parameter is given by parameter values such that the change in the likelihood function is one-half.
\begin{table}[htbp]
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
Model &$N_{\text{dof}}$& Normalization & Function  \\
\hline
P &1& $(\tg-1)\temin^{\tg-1}$& $E^{-\tg}$ \\
\hline
DP &3&  
$\frac{\tg-1}{\teb} 
\left\{\left(\frac{\teb}{\temin}\right)^{\tg-1} + 
             \frac{\tg-1}{\td-1} - 1 \right\}^{-1}$ & 
\begin{tabular}{c c}
$\left(\frac{E}{\teb}\right)^{-\tg}$ &$\temin \leq E < \teb$ \\
$\left(\frac{E}{\teb}\right)^{-\td}$ & $\teb \leq E$
\end{tabular} \\
\hline
\end{tabular}
\end{center}
\caption[The model designation 
  (Model = Pure power-law or Double Power-law), 
  number of free parameters, normalization, and form of the 
  function used to fit the fluxes used in this study.]{\label{tab:models}
  The model designation 
  (Model = Pure power-law or Double Power-law), 
  number of free parameters, normalization, and form of the 
  function used to fit the fluxes used in this study.
}
\end{table}

%% -----------------------------------------
\subsection{``Angles''} \label{sec:Angles}
Associated with each energy is, of course, the arrival direction of the CR. 
We study the energy spectrum in five different angular coordinates. 
For each angle we bin the events into (approximately) equal event number bins 
and fit the energy flux of 
the events in each bin to both the pure and double power-laws.
We use four bins with $\sim4350$ events per bin for each of these angles.
The angles we study can be divided into two classes, they are: 
\begin{itemize}
\item {\bf The Local Frame} This coordinate system is the natural one for the 
  observatory. 
  \begin{itemize}
    \item {\it Zenith} angle $0\degg\leq\theta\leq60\degg$. The determination of the energy, 
      see \secref{sec:Data}, is directly dependent on $\theta$.
    \item {\it Azimuthal} angle $-180\degg<\phi\leq180\degg$, measured from due north.
    \item {\it Time} of arrival 26/1/04$\leq$D/M/Y$\leq$1/6/08.
  \end{itemize}
  In principle, we expect these fluxes to be independent of the angular bin 
  and they therefore offer a good cross check for the data. 
\item {\bf The Global Frames} These coordinate systems are based on astronomically 
  convenient parametrizations of the sphere. 
  \begin{itemize}
    \item Equatorial coordinates, {\it declination} 
      $-90\degg<\text{Dec}\leq 25\degg$.
    \item Galactic plane,{\it magnitude of the  galactic latitude} 
      $0\degg<|b_{\text{G}}|\leq 90\degg$.
    \item Super-Galactic plane, {\it magnitude of the super-galactic latitude} 
      $0\degg<|b_{\text{SG}}|\leq 90\degg$.
    \item Centaurus A, {\it angular distance from Centaurus A} 
      $0\degg<|\text{Cent A}|\leq 180\degg$.
  \end{itemize}
\end{itemize}

%% -----------------------------------------
\subsection{Metrics} \label{sec:Metrics}
We know that the single power-law is a bad fit to the data, see \cite{ref:PAOprl}, 
and we are therefore most interested in the characteristic location and shape 
of the flux of the highest energy events.

The three parameters in the double power-law fit can describe a flux with 
a sharp cut-off but they all conspire to effect the high energy flux. 
Therefore, we use as our first metric, a combination of the double power-law parameters. 
The energy in EeV at which the double power-law model reaches half the value it would have if
the low energy index continued above $\teb$ is given by 
\begin{linenomath*}
  \begin{equation} \label{equ:Eh}
    \teh = 2^{\frac{1}{\td-\tg}} \teb.
  \end{equation}
\end{linenomath*}
Since it depends on $\teb$ and $\td-\tg$, this parameter includes information 
about both the location and the shape of a particular double power-law parameter set. 
Writing the error matrix for the parameters $\teb$, $\td$ and $\tg$ as 
\begin{linenomath*}
  \begin{equation} \label{equ:sigma}
    \left( 
      \begin{array}{ccc} 
        \sigma^{2}_{\tg} & \sigma_{\tg}\sigma_{\teb} & \sigma_{\tg}\sigma_{\td} \\
        \sigma_{\tg}\sigma_{\teb} & \sigma^{2}_{\teb} & \sigma_{\teb}\sigma_{\td} \\
        \sigma_{\tg}\sigma_{\td} & \sigma_{\teb}\sigma_{\td} & \sigma^{2}_{\td}
      \end{array} 
      \right), 
  \end{equation}
\end{linenomath*}
we can write the statistical error on $\teh$ as 
\begin{linenomath*}
  \begin{equation} \label{equ:dEh}
    \left( \frac{\sigma_{\teh}}{\teh} \right)^{2} 
    = \left( \frac{\sigma_{\teb}}{\teb} \right)^{2} - 
      \frac{2(\sigma_{\teb}\sigma_{\td}-\sigma_{\tg}\sigma_{\teb})}{\teb}
      \left( \frac{\lg 2}{(\td-\tg)^{2}} \right) \nonumber \\ 
      + (\sigma^{2}_{\tg}  - 2\sigma_{\tg}\sigma_{\td} + \sigma^{2}_{\td}) 
      \left( \frac{\lg 2}{(\td-\tg)^{2}} \right)^{2}.
  \end{equation}
\end{linenomath*}
By calculating the error weighted average of $\teh$ over the bins of each angle we 
obtain a $\chi^{2}$/DoF for each angle. If this value is near unity we 
can be reasonably sure that the spectra are consistent within each angle. 

The second metric we use is motivated solely by ``expected'' physics; 
we tabulate the number of events with energy greater than 56EeV $\Ngtr$ for each 
bin of each angle. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion} \label{sec:Results}
In Figs. \ref{fig:cCDFloc} and \ref{fig:cCDFglob} we plot a representation 
of the flux for each bin of the angles studied. 
For each event in a set we plot the $\log_{10}$ of the energy along the horizontal axis. 
Along the vertical axis we plot the $\log_{10}$ of the ``cumulative flux'' $\Phi$ times a 
scale factor $E^{2}_{(k)}$. 
Let $E_{(1)}< E_{(2)}< \ldots < E_{(k)} < \ldots < E_{(N)}$ be the ordered set of energies 
under consideration. For the $k^{\text{th}}$ event energy $E_{(k)}$ we plot 
\begin{linenomath*}
  \begin{equation} \label{equ:Phi}
    \lg(E^{2}_{(k)} \Phi) = \lg\left( E^{2}_{(k)} \frac{N-k+1}{\Omega} \right), 
  \end{equation}
\end{linenomath*}
where $\Omega = 7000 \,\text{km}^{2}\,\text{sr}\,\text{yr}$ is the exposure quoted 
in \cite{ref:PAOprl}, $N$ is the total number of events in the set being plotted and 
$k$ is the event's {\it rank}. 
These types of {\it rank-frequency} plots allow one a detailed view of the flux of the 
highest energy events. 
In each of the plots in Figs. \ref{fig:cCDFloc} and \ref{fig:cCDFglob} we show the flux of all of the 
17,361 events that satisfy our cut criteria in solid black. 
The flux for each bin of the angle under consideration is plotted in broken and colored lines. 

The numerical results of fitting the models to the fluxes in each angular bin are 
summarized in Tables \ref{tab:loc} and \ref{tab:glob}. 
The bin-widths, chosen such that the number of events in each bin is $\sim4350$, 
are listed in the first column of the tables. 
In the second column we tabulate the best fit spectral index $\tg$ of the 
single power-law fit to each bin. 
The next three columns list the best fit double power-law parameters $\tg$, $\teb$ and $\td$.  
The $\chi^{2}/$DoF for all of these fits are summarized in \tabref{tab:Chi2}. 
\begin{table}[htbp]
\begin{center}
\begin{tabular}{|c||c|c|}
\hline
Angle     &  $\bar{\teh}$ & $\chi^{2}/$DoF \\
\hline
\hline
$\theta$ & $55\pm2$ & $3.26/3 = 1.09$ \\
\hline
$\phi$ & $54\pm2$ & $30.25/3 = 10.08$ \\
\hline
$\text{Time}$ & $55\pm2$ & $8.03/3 = 2.68$ \\
\hline
$\text{UTC}$ & $54\pm2$ & $2.86/3 = 0.95$ \\
\hline
$\text{NSec}$ & $53\pm2$ & $22.55/3 = 7.52$ \\
\hline
$\text{NSecMod}$ & $54\pm2$ & $20.33/3 = 6.78$ \\
\hline
$\text{Sec}$ & $55\pm2$ & $8.04/3 = 2.68$ \\
\hline
$\text{SecMod}$ & $53\pm3$ & $2.96/3 = 0.99$ \\
\hline
$\text{Dec}$ & $52\pm1$ & $9.79/3 = 3.26$ \\
\hline
$\bgalm$ & $57\pm2$ & $2.66/3 = 0.89$ \\
\hline
$\bsgalm$ & $55\pm2$ & $5.33/3 = 1.78$ \\
\hline
$\text{Cent A}$ & $53\pm1$ & $11.46/3 = 3.82$ \\
\hline
\end{tabular}
\end{center}
\caption[The error weighted average $\bar{\teh}$ for each ``angle'' studied.]{\label{tab:Chi2}
The error weighted average $\bar{\teh}$ for each ``angle'' studied.
The errors are statistical. See \secref{sec:Method} for the description
of the models and methods.}
\end{table}

The zenith angle is probably the most important of the angles to check for 
consistency since the energy is so intimately tied to it, see \equref{equ:E}. 
In our analysis we demand only that there be four bins with approximately 
equal numbers of events. 
The fact that the $2\text{nd}$ and $3\text{rd}$ zenith bins 
meet at $\sim38\degg$ means that there are approximately the same number of events 
with $\theta<38\degg$ as there are with $\theta>38\degg$. 
This is consistent with the method of reconstruction, 
i.e. the ``isotropy'' assumption in \secref{sec:Data}, for events with $E\geq10^{0.5}$EeV (see also \chpref{chap:tpao}). 

All of the other coordinates show little difference between the spectra observed in each bin, with two exceptions.
The first is the azimuthal coordinate (i.e. the cardinal of the arrival direction). 
The events in the $-3\degreee < \phi < 89\degreee$ bin have a markedly higher characteristic cutoff energy $\teh$. 
This means that there are somewhat more ultra high energy events arriving from the north-west quadrant of the 
local coordinate system\footnote{This is the coordinate system one uses with a compass!}. 
We know of no physical explanation for this deviation -- CR ``should be'' isotropic in this coordinate at this energy. 
This excess flux could be due to a statistical fluctuation -- $\Ngtr$ is a meager 14 for this quadrant -- 
or, possibly, an anomaly in the reconstruction algorithm. 

The second anomalous $\teh$ measurement occurs within $44\degreee$ of Cen A.
This excess flux would be generally consistent with an excess of high energy events arriving from a source near Cen A. 
Indeed, much work is underway within Auger to observe such an excess via traditional angular methods. 
The excess reported here is, however, called into question by the anomalous flux in the north-west cardinal coordinate 
and observers should take care to check for anisotropies in all coordinate systems.   

\begin{sidewaystable}[htbp]
\begin{center}
\begin{tabular}{|c||c||c||c|c|c||c||c|}
\hline
``Angle'' & Interval & $\tg$ & $\tg$ & $\teb$/EeV & $\td$ & $\teh$/EeV & $\Ngtr$ \\
\hline
\hline
\multirow{4}{*}{Zenith} & $  0\degg \leq \theta <  27\degg$ & $2.78\pm0.03$ & $2.72\pm0.03$ & $30\pm 4$ & $3.69\pm0.25$ & $ 61\pm  6$ & $ 11$\\ 
\cline{2-8}
 &$ 27\degg \leq \theta <  38\degg$ & $2.75\pm0.03$ & $2.69\pm0.03$ & $40\pm 4$ & $4.52\pm0.47$ & $ 59\pm  4$ & $ 11$\\ 
\cline{2-8}
 &$ 38\degg \leq \theta <  49\degg$ & $2.83\pm0.03$ & $2.77\pm0.03$ & $38\pm 3$ & $4.86\pm0.55$ & $ 52\pm  3$ & $  7$\\ 
\cline{2-8}
 &$ 49\degg \leq \theta <  60\degg$ & $2.83\pm0.03$ & $2.78\pm0.03$ & $37\pm 5$ & $4.25\pm0.42$ & $ 59\pm  7$ & $  7$\\ 
\hline
\hline
\multirow{4}{*}{Azimuth} &$-180\degg \leq \phi < -92\degg$ & $2.75\pm0.03$ & $2.67\pm0.03$ & $29\pm 3$ & $4.13\pm0.30$ & $ 47\pm  3$ & $  8$\\ 
\cline{2-8}
 &$-92\degg \leq \phi <  -3\degg$ & $2.82\pm0.03$ & $2.76\pm0.03$ & $38\pm 3$ & $4.72\pm0.53$ & $ 55\pm  2$ & $  7$\\ 
\cline{2-8}
 &$ -3\degg \leq \phi <  89\degg$ & $2.80\pm0.03$ & $2.77\pm0.03$ & $58\pm11$ & $4.60\pm0.72$ & $ 84\pm  6$ & $ 14$\\ 
\cline{2-8}
 &$ 89\degg \leq \phi < 180\degg$ & $2.82\pm0.03$ & $2.76\pm0.03$ & $31\pm 3$ & $3.95\pm0.30$ & $ 55\pm  4$ & $  7$\\ 
\hline
\hline
\multirow{4}{*}{Time} &$28/ 1/2004 \leq \text{D/M/Y} <  7/ 3/2006$ & $2.77\pm0.03$ & $2.71\pm0.03$ & $35\pm 3$ & $4.23\pm0.38$ & $ 56\pm  3$ & $ 10$\\ 
\cline{2-8}
 &$ 7/ 3/2006 \leq \text{D/M/Y} < 16/ 2/2007$ & $2.80\pm0.03$ & $2.73\pm0.03$ & $31\pm 3$ & $4.15\pm0.33$ & $ 51\pm  3$ & $  6$\\ 
\cline{2-8}
 &$16/ 2/2007 \leq \text{D/M/Y} < 18/10/2007$ & $2.82\pm0.03$ & $2.77\pm0.03$ & $37\pm 3$ & $4.43\pm0.45$ & $ 56\pm  3$ & $ 10$\\ 
\cline{2-8}
 &$18/10/2007 \leq \text{D/M/Y} <  1/ 6/2008$ & $2.79\pm0.03$ & $2.75\pm0.03$ & $40\pm 6$ & $4.03\pm0.40$ & $ 69\pm  6$ & $ 10$\\ 
\hline
\hline
\multirow{4}{*}{Dec} &$-90\degg \leq \Dec < -50\degg$ & $2.76\pm0.03$ & $2.69\pm0.03$ & $38\pm 3$ & $5.15\pm0.57$ & $ 50\pm  2$ & $  6$\\ 
\cline{2-8}
 &$-50\degg \leq \Dec < -30\degg$ & $2.80\pm0.03$ & $2.75\pm0.03$ & $38\pm 3$ & $4.35\pm0.44$ & $ 59\pm  4$ & $  9$\\ 
\cline{2-8}
 &$-30\degg \leq \Dec < -10\degg$ & $2.82\pm0.03$ & $2.76\pm0.03$ & $25\pm 3$ & $3.47\pm0.19$ & $ 66\pm  9$ & $ 11$\\ 
\cline{2-8}
 &$-10\degg \leq \Dec <  25\degg$ & $2.81\pm0.03$ & $2.75\pm0.03$ & $35\pm 4$ & $4.06\pm0.36$ & $ 59\pm  4$ & $ 10$\\ 
\hline
\end{tabular}
\end{center}
\caption[The best fit model parameters for each bin of the Theta coordinate.]{\label{tab:loc}
The best fit model parameters for each bin of the Theta coordinate.
The errors are statistical. See \secref{sec:Method} for the description
of the models and methods.}
\end{sidewaystable}

\begin{sidewaysfigure}[htbp]
	\begin{center}
		\begin{tabular}{c c}
			\includegraphics[height=8.25cm, width=0.5\linewidth]
			{Figs/cEhalfTheta_doublepowerlaw} & 
			\includegraphics[height=8.25cm, width=0.5\linewidth]
			{Figs/cEhalfPhi_doublepowerlaw} \\
			\includegraphics[height=8.25cm, width=0.5\linewidth]
			{Figs/cEhalfTime_doublepowerlaw} &
		\end{tabular}
	\end{center}
	\caption[The value and statistical error of $\teh$, see \equref{equ:Eh} and \tabref{tab:Chi2}, for the local coordinates.]{\label{fig:Ehsloc}
    The value and statistical error of $\teh$, see \equref{equ:Eh} and \tabref{tab:Chi2}, for the local coordinates.}
\end{sidewaysfigure}

\begin{sidewaysfigure}[htbp]
	\begin{center}
		\begin{tabular}{c c}
	      \includegraphics[height=8.25cm, width=0.5\linewidth]
	                      {Figs/cCDFTheta} &
	      \includegraphics[height=8.25cm, width=0.5\linewidth]
	                      {Figs/cCDFPhi} \\ 
	      \includegraphics[height=8.25cm, width=0.5\linewidth]
	                      {Figs/cCDFTime}  &
		\end{tabular}
	\end{center}
	\caption[The cumulative flux, see \equref{equ:Phi}, for the local coordinates.]{\label{fig:cCDFloc}
    The cumulative flux, see \equref{equ:Phi}, for the local coordinates.}
\end{sidewaysfigure}


\begin{sidewaystable}[htbp]
\begin{center}
\begin{tabular}{|c||c||c||c|c|c||c||c|}
\hline
``Angle'' & Interval & $\tg$ & $\tg$ & $\teb$/EeV & $\td$ & $\teh$/EeV & $\Ngtr$ \\
\hline
\hline
\multirow{4}{*}{Dec} &$-90\degg \leq \Dec < -50\degg$ & $2.76\pm0.03$ & $2.69\pm0.03$ & $38\pm 3$ & $5.15\pm0.57$ & $ 50\pm  2$ & $  6$\\ 
\cline{2-8}
 &$-50\degg \leq \Dec < -30\degg$ & $2.80\pm0.03$ & $2.75\pm0.03$ & $38\pm 3$ & $4.35\pm0.44$ & $ 59\pm  4$ & $  9$\\ 
\cline{2-8}
 &$-30\degg \leq \Dec < -10\degg$ & $2.82\pm0.03$ & $2.76\pm0.03$ & $25\pm 3$ & $3.47\pm0.19$ & $ 66\pm  9$ & $ 11$\\ 
\cline{2-8}
 &$-10\degg \leq \Dec <  25\degg$ & $2.81\pm0.03$ & $2.75\pm0.03$ & $35\pm 4$ & $4.06\pm0.36$ & $ 59\pm  4$ & $ 10$\\ 
\hline
\hline
\multirow{4}{*}{Gal. Lat.} &$  0\degg \leq \bgalm <  14\degg$ & $2.80\pm0.03$ & $2.75\pm0.03$ & $43\pm 4$ & $4.81\pm0.60$ & $ 60\pm  3$ & $  8$\\ 
\cline{2-8}
 &$ 14\degg \leq \bgalm <  30\degg$ & $2.78\pm0.03$ & $2.72\pm0.03$ & $39\pm 3$ & $4.52\pm0.47$ & $ 58\pm  3$ & $ 11$\\ 
\cline{2-8}
 &$ 30\degg \leq \bgalm <  48\degg$ & $2.80\pm0.03$ & $2.74\pm0.03$ & $31\pm 3$ & $3.83\pm0.29$ & $ 59\pm  5$ & $ 10$\\ 
\cline{2-8}
 &$ 48\degg \leq \bgalm <  90\degg$ & $2.80\pm0.03$ & $2.74\pm0.03$ & $35\pm 3$ & $4.31\pm0.39$ & $ 54\pm  3$ & $  7$\\ 
\hline
\hline
\multirow{4}{*}{S. Gal. Lat.} &$  0\degg \leq \bsgalm <  14\degg$ & $2.81\pm0.03$ & $2.76\pm0.03$ & $28\pm 4$ & $3.59\pm0.23$ & $ 64\pm  7$ & $ 12$\\ 
\cline{2-8}
 &$ 14\degg \leq \bsgalm <  30\degg$ & $2.78\pm0.03$ & $2.72\pm0.03$ & $37\pm 5$ & $4.13\pm0.38$ & $ 60\pm  4$ & $  9$\\ 
\cline{2-8}
 &$ 30\degg \leq \bsgalm <  49\degg$ & $2.79\pm0.03$ & $2.72\pm0.03$ & $38\pm 2$ & $4.92\pm0.54$ & $ 52\pm  2$ & $  7$\\ 
\cline{2-8}
 &$ 49\degg \leq \bsgalm <  90\degg$ & $2.81\pm0.03$ & $2.75\pm0.03$ & $34\pm 4$ & $4.21\pm0.38$ & $ 55\pm  3$ & $  8$\\ 
\hline
\hline
\multirow{4}{*}{Cent A} &$  0\degg \leq \text{Cent A} <  44\degg$ & $2.80\pm0.03$ & $2.76\pm0.03$ & $38\pm 5$ & $3.88\pm0.35$ & $ 70\pm  6$ & $ 14$\\ 
\cline{2-8}
 &$ 44\degg \leq \text{Cent A} <  67\degg$ & $2.78\pm0.03$ & $2.71\pm0.03$ & $37\pm 3$ & $5.22\pm0.59$ & $ 49\pm  2$ & $  3$\\ 
\cline{2-8}
 &$ 67\degg \leq \text{Cent A} <  93\degg$ & $2.83\pm0.03$ & $2.77\pm0.03$ & $38\pm 4$ & $4.79\pm0.55$ & $ 54\pm  2$ & $  6$\\ 
\cline{2-8}
 &$ 93\degg \leq \text{Cent A} < 180\degg$ & $2.77\pm0.03$ & $2.70\pm0.03$ & $26\pm 2$ & $3.54\pm0.20$ & $ 58\pm  6$ & $ 13$\\ 
\hline
\end{tabular}
\end{center}
\caption[The best fit model parameters for each bin of the Theta coordinate.]{\label{tab:glob}
The best fit model parameters for each bin of the Theta coordinate.
The errors are statistical. See \secref{sec:Method} for the description
of the models and methods.}
\end{sidewaystable}

\begin{sidewaysfigure}[htbp]
	\begin{center}
		\begin{tabular}{c c}
	      \includegraphics[height=8.25cm, width=0.5\linewidth]
	                      {Figs/cEhalfDec_doublepowerlaw} &
	      \includegraphics[height=8.25cm, width=0.5\linewidth]
	                      {Figs/cEhalfgalBmag_doublepowerlaw} \\ 
	      \includegraphics[height=8.25cm, width=0.5\linewidth]
	                      {Figs/cEhalfsgalBmag_doublepowerlaw}  &
	      \includegraphics[height=8.25cm, width=0.5\linewidth]
	                      {Figs/cEhalfCentA_doublepowerlaw} 
		\end{tabular}
	\end{center}
	\caption[The value and statistical error of $\teh$, see \equref{equ:Eh} and \tabref{tab:Chi2}.]{\label{fig:Ehsglob}
    The value and statistical error of $\teh$, see \equref{equ:Eh} and \tabref{tab:Chi2}.}
\end{sidewaysfigure}

\begin{sidewaysfigure}[htbp]
	\begin{center}
		\begin{tabular}{c c}
	      \includegraphics[height=8.25cm, width=0.5\linewidth]
	                      {Figs/cCDFDec} &
	      \includegraphics[height=8.25cm, width=0.5\linewidth]
	                      {Figs/cCDFgalBmag} \\ 
	      \includegraphics[height=8.25cm, width=0.5\linewidth]
	                      {Figs/cCDFsgalBmag}  &
	      \includegraphics[height=8.25cm, width=0.5\linewidth]
	                      {Figs/cCDFCentA} 
		\end{tabular}
	\end{center}
	\caption[The cumulative flux, see \equref{equ:Phi}, for the global coordinates.]{\label{fig:cCDFglob}
    The cumulative flux, see \equref{equ:Phi}, for the global coordinates.}
\end{sidewaysfigure}


