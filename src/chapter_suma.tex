% chapter_suma.tex {Summary}
\chapter{Summary} \label{chap:suma}

This thesis presents an exploration of two issues with fundamental importance to the study of high energy CR: the energy spectrum, and anisotropy in the CR sky.  As an introduction to these topics, the theoretical context for the observation of the energy spectrum and arrival directions is discussed 
in \chpref{chap:cont}. 
An outline of the particle physics phenomenology of air showers created by CR entering the atmosphere of Earth 
is given in \secref{tpao:sec:eas}, while the remaining sections of \chpref{chap:tpao} are devoted to the 
reconstruction of the energy and arrival direction of the CR primary from data collected at the Pierre Auger Observatory.

In \chpref{chap:fsba} two separate statistical tests are applied to both  AGASA and preliminary Auger Cosmic Ray Energy spectra 
in an attempt to find deviation from a pure power-law. The first test is constructed from the probability distribution 
for the maximum event of a sample drawn from a power-law. The second employs the TP-statistic, a function defined 
to deviate from zero when the sample deviates from the power-law form, regardless of the value of the power index. 
The AGASA data show no significant deviation from a power-law when subjected to both tests.
Applying these tests to the Auger spectrum suggests deviation from a power-law. However, potentially 
large systematics on the relative energy scale prevent us from drawing definite conclusions at this time.

Un-binned statistical tools for analyzing the cosmic ray energy spectrum 
are developed and illustrated with a simulated data set in \chpref{chap:fsua}.
These tools are designed to extract accurate and precise model parameter 
estimators in the presence of statistical and systematic energy errors.
Two robust methods are used to test for the presence of flux suppression 
at the highest energies: the Tail-Power statistic and a likelihood ratio test. 
Both tests supply evidence of flux suppression in the simulated data. 
The tools presented can be generalized for use on any astrophysical data set 
where the power-law assumption is relevant and can be used to aid observational design. 

In \chpref{chap:fsua} the spectrum in six different ``angular'' coordinates is studied; 
zenith, azimuth, time, declination, angular distance from the 
galactic plane, the super galactic planes and Centaurus A. 
These events are binned by angle into equal event number bins 
and the fluxes fit to single and double power-laws. 
With a concentration on the characterization of the consistency of the 
highest energy flux between the angular bins, anomalous high energy flux for the azimuth between 
-3 and 89 degrees and within 44 degrees of Centaurus A is found.

\chpref{chap:elp} relates a manuscript written just after the so-called ``running prescription'' was passed\cite{antoine}, the results of which the Auger collaboration relied upon in the publication, ``the correlation of ultra high energy CR with Active Galactic Nuclei''\cite{refPAOscience}. 
The primary goal of this work is the estimation of the observed value of the correlation {\em little-p} in post-scan data.  Although not widely used at the time of its initial release, this work has since become the analysis-basis for the results of some of the later Auger anisotropy work (see \chpref{chap:agn}). 

The two-point angular correlation function is a traditional method to search for deviations from expectations of isotropy. 
In \chpref{chap:ss}, a statistically descriptive three-point method is developed and explored, with the intended application being the 
search for deviations from isotropy in the highest energy cosmic rays.  
(The background and documentation for this three-point method can be found in \chpref{chap:cim})
As part of this work, the sensitivity of the so-called ``shape-strength'' method is compared to that of a two-point method for a variety of 
Monte-Carlo simulated anisotropic signals.  
In addition, studies are performed with anisotropic source signals diluted by an isotropic background. 
Type I and II errors for rejecting the hypothesis of isotropic cosmic ray arrival directions are evaluated for four
different event sample sizes: 27, 40, 60 and 80 events, consistent with near-term data expectations from the Pierre Auger Observatory. 
In all cases, the ability to reject the isotropic hypothesis improves with event size and with the fraction of anisotropic signal. 
But while data sets with $\sim 40$ events should be sufficient for reliable identification of anisotropy, in cases of rather extreme
(highly anisotropic) data, much larger data sets are suggested for reliable identification of more subtle anisotropies.
The shape-strength method consistently performs better than the two point method and can be easily adapted to an arbitrary experimental 
exposure on the celestial sphere. 
 
Together, the analysis tools developed in this thesis afford a much more precise view of high energy CR, their characteristic energies, and their origins.  Armed with these tools, the Auger collaboration--and the scientific community, in general--is now in a position to more fully understand the nature of these energetic particles from outer space.













