% chapter_agn.tex {Anisotropy : Correlation with AGN}
\chapter{Anisotropy : \\ Correlation with AGN} \label{chap:agn}

\section{Chapter Overview} 
The text in this chapter has been submitted to the proceeding of the 
31$^{\text{st}}$ annual International Cosmic Ray Conference (ICRC) in {\L}\'{o}d\'{z}, Poland under the 
title ``Correlation of the Highest Energy Cosmic Rays with Nearby Extragalactic Objects in Pierre Auger 
Observatory Data''\cite{agnhague}. 
I am the ``primary author'' of this text (including figures and numerical values), 
but it the final language and content are the result of a massive, active and quite essential collaboration. 
Indeed, much of the actual language is a balance of many -- often divergent -- opinions. 
In producing this document (chapter) I learned as much about the collaborative endeavor as the scientific. 

The complete text is included here as an augmentation to \chpref{chap:elp}; 
to show how the estimation of little-$p$ (simply $p$ below) is incorporated into a 
the complete update if the AGN signal\cite{refPAOscience,Abraham:2007si}. 
In particular the work of \chpref{chap:elp} is used to create the right panel of \figref{fig:seq}. 
The final text is reviewed in \secref{agn:sec:cps}.

{\bf Abstract:} We update the analysis of correlation between the arrival directions of the highest energy cosmic rays 
observed by the Pierre Auger Observatory and the positions of nearby active galaxies. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}\label{sec:intro}
Using data collected between \dayone~and \daysci, the Pierre Auger Observatory has reported \cite{refPAOscience,Abraham:2007si} evidence of anisotropy 
in the arrival directions of cosmic rays (CR) with energies exceeding $\sim 60$~EeV ($1$~EeV is $10^{18}$~eV).
The arrival directions were correlated with the positions of nearby objects from the 12th edition of the catalog of 
quasars and active galactic nuclei (AGN) by V\'{e}ron-Cetty and V\'{e}ron \cite{refVCV} (VCV catalog). 
This catalog is not an unbiased statistical sample, since it is neither homogeneous nor statistically complete. 
This is not an obstacle to demonstrating the existence of anisotropy if CR arrive preferentially close to the positions of 
nearby objects in this sample. 
The nature of the catalog, however, limits the ability of the correlation method to identify the actual sources of cosmic rays. 
The observed correlation identifies neither individual sources nor a specific class of astrophysical sites of origin. 
It provides clues to the extragalactic origin of the CR with the highest energies and suggests that the suppression of the 
flux (see \cite{ref:PAOprl} and \cite{Abbasi:2007sv}) is due to interaction with the cosmic background radiation.

In this article we update the analysis of correlation with AGN in the VCV catalog by including data collected through \dayend. 
We also analyse the distribution of arrival directions with respect to 
the location of the Centaurus cluster and the radio source Cen A.
Alternative tests that may discriminate among different populations of source candidates are presented in a separate paper at 
this conference \cite{julien}. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data}\label{sec:data}
The data set analyzed here consists of events observed by the Pierre Auger Observatory prior to \dayend. 
We consider events with zenith angles smaller than $60\degreee$. 
The event selection implemented in the present analysis requires that at least five active nearest-neighbors surround the station with 
the highest signal when the event was recorded, and that the reconstructed shower core be inside an active equilateral 
triangle of detectors. 
The integrated exposure for this event selection amounts to 17040~km$^{2}$~sr~yr ($\pm 3\%$), 
nearly twice the exposure used in \cite{refPAOscience,Abraham:2007si}. 

In \cite{refPAOscience,Abraham:2007si} we published the list of 27 events with $E>57$~EeV. 
Since then, the reconstruction algorithms and calibration procedures of the Pierre Auger Observatory have been updated. 
The lowest energy among these same 27 events is 55 EeV according to the latest reconstruction. 
Reconstructed values for the arrival directions of these events differ by less than $0.1\degreee$ from their previous determination. 
There are now 31 additional events above the energy threshold of 55 EeV. 
The systematic uncertainty of the observed energy for events used here is $\sim 22\%$ 
and the energy resolution is $\sim 17\%$ \cite{Schussler,Giulio}. 
The angular resolution of the arrival directions for events with energy 
above this threshold is better than $0.9\degreee$ \cite{Bonifazi}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Update of the correlation with AGN}\label{sec:uca}
To avoid the negative impact of trial factors in {\em a posteriori} analyses, the statistical significance of the anisotropy 
reported in \cite{refPAOscience,Abraham:2007si} was established through a test with independent data. 
The parameters of the test were chosen by an exploratory scan using events observed prior to \dayscan. 
The scan searched for a correlation of CR with objects in the VCV catalog with redshift less than $\zm$ at an angular 
scale $\psm$ and energy threshold $\ethr$.
The scan was implemented to find a minimum of the probability $P$ that $k$ or more out of a total of $N$ events from an isotropic flux 
are correlated by chance with the selected objects at the chosen angular scale, given by
\begin{equation}
	P=\sum_{j=k}^N \left(\begin{array}{c} N \\ j \end{array}\right) \piso^{\/j}(1-\piso)^{N-j}~.
	\label{equ:P}
\end{equation}
We take $\piso$ to be the exposure-weighted fraction of the sky accessible to the Pierre Auger Observatory 
that is within $\psm$ degrees of the selected potential sources. 
The minimum value of $P$ was found for the parameters
$\psm=3.1\degreee$, $\zm=0.018$ and  $\ethr=55$~EeV (in the present energy calibration).
The probability that an individual event from an isotropic flux arrives within the fraction of the sky 
prescribed by these parameters by chance is $\piso=0.21$.
\begin{figure*}[ht]
	\begin{center}    
		\begin{tabular}{cc}
			\includegraphics[width=0.480\linewidth]{Figs/icrc0143_fig01a} &
			\includegraphics[width=0.480\linewidth]{Figs/icrc0143_fig01b} 
		\end{tabular}
	\end{center}
	\caption[Monitoring the correlation signal.]{
	\label{fig:seq}Monitoring the correlation signal. 
	{\em Left:} The sequential analysis of cosmic rays with energy greater than 55~EeV arriving after \dayscan. 
	The likelihood ratio $\log_{10} R$ (see \equref{equ:R}) for the data is plotted in black circles. 
	Events that arrive within \thepsm of an AGN with maximum redshift \thezm result in an up-tick of this line. 
	Values above the area shaded in blue have less than 1\% chance probability to arise from an isotropic distribution 
	($\piso=0.21$).
	{\em Right:} The most likely value of the binomial parameter $\psig=k/N$ is plotted with black circles as 
	a function of time.
	The $1\sigma$ and $2\sigma$ uncertainties in the observed value are shaded. 
	The horizontal dashed line shows the isotropic value $\piso = 0.21$. 
	The current estimate of the signal is $0.38 \pm 0.07$. 
	In both plots events to the left of the dashed vertical line correspond to period II of 
	\tabref{tab:sum} and those to the right, collected after \cite{refPAOscience,Abraham:2007si}, correspond to period III.
	}
\end{figure*}
\begin{table*}[ht]
	\caption[A numerical summary of results for events with $E\geq 55$~EeV.]{\label{tab:sum}
			A numerical summary of results for events with $E\geq 55$~EeV.
			See the text for a description of the entries.} 
	\begin{center}
		\begin{tabular}{|c|c|c|c|c|c|c|c|}
			\hline
			Period & Exposure & GP & $N$ & $k$ & $k_{\text{iso}}$ & $P$ \\
			\hline
			\multirow{2}{*}{I} & \multirow{2}{*}{4390} & unmasked & 14 &  9 & 2.9 &  \\
			                                         & &  masked & 10 &  8 & 2.5 & \\
			\hline
			\multirow{2}{*}{II} & \multirow{2}{*}{4500} & unmasked & 13 &  9 & 2.7 & $2\times 10^{-4}$ \\
				                                      & &  masked & 11 &  9 & 2.8 & $1\times 10^{-4}$ \\
			\hline
			\multirow{2}{*}{III} & \multirow{2}{*}{8150} & unmasked & 31 &  8 & 6.5 & 0.33 \\
				                                       & &  masked & 24 &  8 & 6.0 & 0.22 \\
			\hline
			\multirow{2}{*}{II+III} & \multirow{2}{*}{12650} & {\bf unmasked} & {\bf 44} & {\bf 17} & {\bf 9.2} & $\mathbf{6\times 10^{-3}}$ \\
				                                           & &  masked & 35 & 17 & 8.8 & $2\times 10^{-3}$ \\
			\hline
			\multirow{2}{*}{I+II} & \multirow{2}{*}{8890} & unmasked & 27 & 18 & 5.7 & \\
				                                        & &  masked & 21 & 17 & 5.3 & \\ 
			\hline
			\multirow{2}{*}{I+II+III} & \multirow{2}{*}{17040} & unmasked & 58 & 26 & 12.2 & \\
				                                             & &  masked & 45 & 25 & 11.3 & \\  
			\hline
		\end{tabular}
	\end{center}
\end{table*}

Of the 27 events observed prior to \daysci, 13 were observed after the exploratory phase. 
Nine of these arrival directions were within the prescribed area of the sky, 
where 2.7 are expected on average if the flux were isotropic. 
This degree of correlation provided a 99\% significance level for rejecting the hypothesis that the 
distribution of arrival directions is isotropic.

The left panel of \figref{fig:seq} displays the likelihood ratio of correlation as a function of the total number of time-ordered 
events observed since \dayscan, i.e. excluding the data used in the exploratory scan that lead to the choice of parameters. 
The likelihood ratio $R$ is defined as (see \cite{wald} and \cite{benz})
\begin{equation}
	R = \frac{\int_{\piso}^1 p^k(1-p)^{N-k}\ dp}{\piso^k(1-\piso)^{N-k+1}} ~.
	\label{equ:R}
\end{equation}
This quantity is the ratio between the binomial probability of correlation 
-- marginalized over its range of possible values and assuming a flat prior -- 
and the binomial probability in the isotropic case ($\piso=0.21$). 
A sequential test rejects the isotropic hypothesis at the 99\% significance level 
(and with less than 5\% chance of incorrectly accepting the null hypothesis) if $R > 95$.  
The likelihood ratio test indicated a 99\% significance level for the anisotropy of the arrival directions 
using the independent data reported in \cite{refPAOscience,Abraham:2007si}. 
Subsequent data neither strengthen the case for anisotropy, nor do they contradict the earlier result.  
The departure from isotropy remains at the 1\% level as measured by the cumulative binomial probability ($P=0.006$), 
with 17 out of 44 events in correlation. 

In the right panel of \figref{fig:seq} we plot the degree of correlation ($\psig$) with objects in the VCV catalog 
as a function of the total number of time-ordered events observed since \dayscan. 
For each new event the best estimate of $\psig$ is $k/N$. 
The $1\sigma$ and $2\sigma$ uncertainties in this value are determined such that 
the area under the posterior distribution function is equal to 68\% and 95\%, respectively. 
The current estimate, with 17 out of 44 events that correlate in the independent data, is $\psig = 0.38$, 
or more than two standard deviations from the value expected from a purely isotropic distribution of events. 
More data are needed to accurately constrain this parameter. 

The correlations between events with $E \geq 55$~EeV and AGN in the 
VCV catalog during the pre- and post- exploratory periods of data collection are summarized in \tabref{tab:sum}.
The left most column shows the period in which the data was collected. 
Period I is the exploratory period from \dayone~through 26 May, 2006. 
The data collected during this period was scanned to establish the parameters which maximize the correlation. 
Period II is from \dayscan~through \daysci~and 
period III includes data collected after \cite{refPAOscience,Abraham:2007si}, from 1 September, 2007 through \dayend. 
The numbers in bold correspond to period II+III and give the results for the post-exploratory 
data (see \figref{fig:seq}).
The exposure for each period is listed in units of km$^{2}$~sr~yr and has an uncertainty of $3\%$. 
If the region of the sky within $12\degreee$ of the galactic plane (GP) is included in the analysis then the third column 
is marked ``unmasked'' (and $\piso=0.21$), if not then it is marked ``masked'' (and $\piso = 0.25$). 
The average number of events from an isotropic flux expected to correlate is listed as $k_{\text{iso}} = N \piso$, 
where $N$ is the total number of events observed during each period. 
$k$ is the number of events that arrive within $3.1\degreee$ of an AGN with a redshift of $0.018$. 
The cumulative binomial probability (see \equref{equ:P}) is shown in the right most column. 
We do not include this value for any row containing period I because this period was used to determine the 
correlation parameters for the rest of the table and cannot, therefore, be interpreted as a statistical significance. 

Note that during period I+II (reported in \cite{refPAOscience,Abraham:2007si}), 
18 out of 27 events arrive within $3.1\degreee$ of an AGN in the VCV catalog with redshift less than $0.018$.
\footnote{Two additional events correlate within a slightly larger angular distance, as reported in \cite{refPAOscience,Abraham:2007si}. 
Here we restrict the analysis to the parameters chosen to monitor the correlation signal.}
There are 31 additional events (during period III) above the specified energy threshold, 8 of which have arrival 
directions within the prescribed area of the sky, 
not significantly more than the 6.5 events that are expected to arrive on average if the flux were isotropic. 

While the degree of correlation with objects in the VCV catalog has decreased with the accumulation of new data,
a re-scan of the complete data set shows that the values of $\psm$, $\zm$ and $\ethr$ that characterise the correlation 
have not changed appreciably from the values reported in \cite{refPAOscience,Abraham:2007si}. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{A posteriori analyses}\label{sec:apa}
In this section we further analyze the complete set of 58 events with energy larger than 55 EeV collected 
before \dayend. 

To complement the information given in \tabref{tab:sum} over different angular scales, 
we plot in \figref{fig:agn} the distribution of angular separations between the arrival directions of the 
58 events with $E > 55$~EeV and the position of the closest object in the VCV catalog within redshift $\zm \le 0.018$. 
The cumulative distribution is plotted in the left panel and the differential distribution is plotted in the right. 
The average distribution expected for 58 events drawn from an isotropic flux is also shown.
In the right panel the 13 events with galactic latitudes $|b| < 12\degreee$ have been shaded. 
Note that only 1 of these 13 events is within $3\degreee$ of a selected AGN. 
Incompleteness of the VCV catalog due to obscuration by the Milky Way or 
larger magnetic bending of CR trajectories along the galactic disk are potential causes for 
smaller correlation of arrival directions at small galactic latitudes.

\begin{figure*}[ht]
	\begin{center}    
		\begin{tabular}{cc}
			\includegraphics[width=0.480\linewidth]{Figs/icrc0143_fig02a} &
			\includegraphics[width=0.480\linewidth]{Figs/icrc0143_fig02b}
		\end{tabular}
	\end{center}
	\caption[The distribution of angular separations between the 58 events with $E>55$~EeV and the closest 
			AGN in the VCV catalog within 75~Mpc.]{The distribution of angular separations between the 58 events with $E>55$~EeV and the closest 
			AGN in the VCV catalog within 75~Mpc.
			{\it Left:} The cumulative number of events as a function of angular distance. 
			The 68\% the confidence intervals for the isotropic expectation is shaded blue.
			{\em Right:} The histogram of events as a function of angular distance. 
			The 13 events with galactic latitudes $|b| < 12\degreee$ are shown with hatching. 
			The average isotropic expectation is shaded brown.
	}\label{fig:agn}
\end{figure*}
\begin{figure*}[th]
	\begin{center}    
		\begin{tabular}{cc}
			\includegraphics[width=0.480\linewidth]{Figs/icrc0143_fig03a} &
			\includegraphics[width=0.480\linewidth]{Figs/icrc0143_fig03b}
		\end{tabular}
	\end{center}
	\caption[The cumulative number of events with $E\geq55$ EeV as a function of angular distance from Cen A. 
		The average isotropic expectation with approximate 68\% confidence intervals is shaded blue.]{{\it Left:} The cumulative number of events with $E\geq55$ EeV as a function of angular distance from Cen A. 
		The average isotropic expectation with approximate 68\% confidence intervals is shaded blue.
		{\em Right:} The histogram of events as a function of angular distance from Cen A.
		The average isotropic expectation is shaded brown. 
	}\label{fig:cena}
\end{figure*}
An excess of events as compared to isotropic expectations is observed from a region of the sky close to the location of 
the radio source Cen A ($\cena$ \cite{neds}).  
In \figref{fig:cena} we plot the distribution of events as a function of angular distance from Cen A. 
In a Kolmogorov-Smirnov \cite{eade} test 2\% of isotropic realizations have maximum departure 
from the isotropic expectation greater than or equal to the maximum departure for the observed events. 
The excess of events in circular windows around Cen A with the smallest isotropic chance probability corresponds to a 
radius of $18\degreee$, which contains 12 events where 2.7 are expected on average if the flux were isotropic. 
The (differential) histogram of angular distances from Cen A is in the right panel of \figref{fig:cena}.

By contrast, the region around the Virgo cluster is densely populated with galaxies but does not have an excess of 
events above isotropic expectations. 
In particular, a circle of radius $20^\circ$ centred at the location of M87 ($\virg$ \cite{neds}) 
does not contain any of the 58 events with energy $E > 55$ EeV. 
This is a region of relatively low exposure for the Pierre Auger Observatory and only 1.2 event is 
expected on average with the current statistics if the flux were isotropic. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion}\label{sec:dis}
With data collected by the Pierre Auger Observatory between \dayone~and \dayend, 
we have updated the analysis reported in \cite{refPAOscience,Abraham:2007si} of correlation between the arrival directions of 
the highest energy cosmic rays and the positions of nearby objects from the 12th edition of the VCV 
catalog of quasars and active galactic nuclei. 
The total number of events above 55~EeV is 58. 
A subset of 44 events are independent of those used to determine the parameters 
($\psm=3.1\degreee$, $\zm=0.018$ and $\ethr=55$~EeV) with which we monitor the correlation signal 
(see \tabref{tab:sum} for more details). 
17 of these 44 events correlate under these parameters. 
This correlation has a less than 1\% probability to occur by chance if the arrival directions are isotropically distributed. 
The evidence for anisotropy has not strengthened since the analysis reported in \cite{refPAOscience,Abraham:2007si}. 
The degree of correlation with objects in the VCV catalog appears to be weaker than suggested by the earliest data.

We note that there is an excess of events in the present data set close to the direction of 
the radio source Cen A, a region dense in potential sources. 
This excess is based on {\em a posteriori} data but suggests that the region of the sky 
near Cen A warrants further study.   

Additional data are needed to make further progress in the quest to identify the sites of ultra high energy CR origin. 
Alternative tests that may discriminate among different populations of source candidates are presented in a separate paper at 
this conference \cite{julien}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Chapter Post-Script}\label{agn:sec:cps}
The biggest (psychological) obstacle for this paper is coping with the fact the the signal strength has 
decreased (drastically) with the accumulation of new data. 
Of course, the new data present a bit of a set back for the most popular current interpretation of the sources 
of CR -- that they originate from {\em VCV} AGN -- and this accounts, I think, for the ``defensive tone'' of the language. 
 
While the picture of a disappearing signal is clear (see \figref{fig:seq}), 
it is worth noting that the presentation strives to emphasize the positive
\footnote{Data is neutral; we are {\em always} biased.} aspects. 
For example, \figref{fig:cena} and \figref{fig:agn} use {\bf all 58} events; 
the events used to establish the correlation parameters are {\bf included}! 
By including the original 27 (pre-scan) events \figref{fig:agn} and any numbers derived from it 
-- derived either numerically or simply by looking at and interpreting the plot -- are {\em biased}. 
This is because nearly half of the CR in the 58 were scanned against the AGN for the most anomalous correlation. 

By definition, Cen A (as in \figref{fig:cena}, or Virgo) is a particular source. 
From an observational perspective one might as well choose {\em any} point in the sky. 
In this case, using all 58 events only includes a bias on the energy threshold $E\sim55$~EeV which 
can be loosely justified by other means, i.e. the GZK-effect. 
Indeed, in preparation for this article many other ``points'' on the sky were studied by the author, including; 
the supergalactic plane, the galactic plane, the equitorial plane, Fornax A, the galactic center etc. 
They were all rejected for publication because they produce null results. 

