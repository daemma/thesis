### ############################################################################
##! @file       Makefile
##! @brief      Recipes for running CMake
##! @author     "Emma Hague" <dev@daemma.io>
##! @date       2019-09-28
##! @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io>
##!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>

BUILDDIR = build

.SILENT : 
.PHONY  : clean rebuild doc
all     : build
bdir    : 
	@test -d $(BUILDDIR) || mkdir $(BUILDDIR)
build   : bdir
	@cd $(BUILDDIR) && \
	cmake ..        && \
	cmake --build . > build.log && \
	cd ..
clean   : 
	@rm -rf $(BUILDDIR)/*
rebuild : clean build


### end Makefile
### ############################################################################
