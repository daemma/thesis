<!--#########################################################################-->
<!-- @file       Readme.md -->
<!-- @brief      Read-me information for the repository containing my thesis. -->
<!-- @author     "Emma Hague" <dev@daemma.io> -->
<!-- @date       2019-09-28 -->
<!-- @copyright  Copyright (C) 2019 "Emma Hague" <dev@daemma.io> -->
<!--             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt> -->
# Thesis
My Ph.D. thesis.

## Dependencies
LaTeX, gnu-make, CMake

## License
[![License](https://img.shields.io/:license-GPLv3-blue.svg)](https://gitlab.com/RSLA/canonical-simulations/blob/4970295be036ecde68eb4ced3d70eb76ba92f636/License.txt) 

thesis is the materials for my Ph.D. thesis

Copyright (C) 2019 "Emma Hague" <dev@daemma.io>,

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.

<!--end Readme.md -->
<!--#########################################################################-->
